# Voting API

This projects implements an API for voting on decisions taken in assemblies of cooperatives.

## Brief
API developed with Spring Boot, uses JPA Hibernate to persist data on a Postgres database.
The data modeling was performed without relationships, as well as database specific functions were not used.
Everything is managed in software, decoupling the database from it.

The software is developed in a layered architecture, therefore, it has layers for controllers, services, DAOs
, entities, etc.

Settings are in application properties file, separated in dev, test and prod environments.

## Prerequisites
To compile this software maven should be installed.
To run this software, a JRE and a database are needed.

## Building
To compile this projects run:
```shell script
mvn clean compile
```
To test this projects run:
```shell script
mvn test
```
To package this projects run:
```shell script
mvn package
```

## Running
To run this software, just call java app as below (make shure you have a database configured in dev profile):
```shell script
java -jar votinng-api.jar
```
Or call docker-compose:
```shell script
docker-compose up
```
## API Usage
Below are the API endpoints to be called, replace the zeroes with the apropriate value.

To create a subject to be voted call:
```shell script
curl --header "Content-Type: application/json" \
  --request POST \
  --data '{ "description": "Test subject" }' \
  https://cgt-voting-api.herokuapp.com/api/v1/subjects
```

Using the subject id from the response above, open a voting session:
```shell script
curl --header "Content-Type: application/json" \
  --request POST \
  --data '{ "subjectId": 0, "duration": 00 }' \
  https://cgt-voting-api.herokuapp.com/api/v1/sessions
```

To vote on a subject, inform the subject, the member's CPF and the vote as below:
```shell script
curl --header "Content-Type: application/json" \
  --request POST \
  --data '{"vote": false,"subjectId": 0,"cpf": "00000000000"}' \
  https://cgt-voting-api.herokuapp.com/api/v1/votes
```

And to get the results (only available after session closes)
```shell script
curl --header "Content-Type: application/json" \
  https://cgt-voting-api.herokuapp.com/api/v1/votes/results/0
```

## Versioning

I use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository
](https://gitlab.com/cassiogt/voting-api/-/tags). 


## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.

## More about this
This project was written for the purpose of showing knowledge on the tools used.
For this, were used:
- Spring Boot, because facilitates the setup process and publication of applications.
- JPA: because allows to save and load Java objects without any DML language. This project doesn't have relations
 between table (e.g @OneToMany, @OneToOne) because it's simple and database decoupled. Of course, performance may not
  be the same, if the software may work only with Postgres databases, a GenerationType.SEQUENCE can be
   used as ID generation strategy, making use of sequence allocation and batch operations also.
- Tests: Some tests uses in memory database and some are using Mock, just to show both ways to do it.
- Swagger: Makes clear the documentation and use of REST APIs.
