FROM adoptopenjdk/openjdk11:alpine-jre
LABEL maintainer="Cássio Tatsch <tatschcassio@gmail.com>"
COPY target/voting-api.jar /app.jar
EXPOSE 8080
CMD ["java", "-Dspring.profiles.active=docker", "-jar", "/app.jar"]
