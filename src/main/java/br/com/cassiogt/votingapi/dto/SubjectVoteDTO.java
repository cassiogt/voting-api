package br.com.cassiogt.votingapi.dto;

import br.com.cassiogt.votingapi.entity.SubjectVote;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.br.CPF;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel
public class SubjectVoteDTO {

    @ApiModelProperty(value = "The vote")
    @NotNull(message = "Vote should not be null")
    private Boolean vote;

    @ApiModelProperty(value = "The subject of the open session")
    @NotNull(message = "Subject ID should not be null")
    private Integer subjectId;

    @ApiModelProperty(value = "The CPF of the member who is voting")
    @NotBlank(message = "CPF should not be blank")
    @CPF(message = "Is not a valid CPF")
    private String cpf;

    public SubjectVote asEntity() {
        return SubjectVote.builder()
                .vote(this.getVote())
                .subjectId(this.getSubjectId())
                .build();
    }
}
