package br.com.cassiogt.votingapi.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@ApiModel
public class VotingResultDTO {

    @Setter
    private SubjectDTO subject;

    private Integer pros = 0;

    private Integer cons = 0;

    public void incrementPros() {
        this.pros++;
    }

    public void incrementCons() {
        this.cons++;
    }
}
