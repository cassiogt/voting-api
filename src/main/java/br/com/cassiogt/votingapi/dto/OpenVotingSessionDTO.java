package br.com.cassiogt.votingapi.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel
public class OpenVotingSessionDTO {

    @ApiModelProperty(value = "ID of the subject which session will be open")
    @NotNull(message = "Subject ID must not be null")
    private Integer subjectId;

    @ApiModelProperty(value = "Period, in minutes, that the session will be open")
    private Integer duration;

}
