package br.com.cassiogt.votingapi.dto;

import br.com.cassiogt.votingapi.entity.Subject;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel
public class SubjectDTO {

    @ApiModelProperty(value = "The subject ID")
    private Integer id;

    @ApiModelProperty(value = "The subject description")
    @NotBlank(message = "Subject must not be empty")
    private String description;

    public Subject asEntity() {
        return Subject.builder()
                .id(this.getId())
                .description(this.getDescription())
                .build();
    }

    public static SubjectDTO fromEntity(Subject subject) {
        return SubjectDTO.builder()
                .id(subject.getId())
                .description(subject.getDescription())
                .build();
    }
}
