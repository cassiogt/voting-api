package br.com.cassiogt.votingapi.dto;

import br.com.cassiogt.votingapi.entity.VotingSession;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDateTime;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel
public class VotingSessionDTO {

    @ApiModelProperty(value = "The ID of the voting session")
    private Integer id;

    @ApiModelProperty(value = "The ID of the subject from current session")
    @NotNull(message = "Subject must not be null")
    private Integer subjectId;

    @ApiModelProperty(value = "The starting time of this session")
    @NotNull
    private LocalDateTime beginDate;

    @ApiModelProperty(value = "The closing time of this session")
    private LocalDateTime dueDate;

    @ApiModelProperty(value = "Status if this session is still open")
    private Boolean open;

    public static VotingSessionDTO fromEntity(VotingSession votingSession) {
        return VotingSessionDTO.builder()
                .id(votingSession.getId())
                .subjectId(votingSession.getSubjectId())
                .beginDate(votingSession.getBeginTime())
                .dueDate(votingSession.getDueTime())
                .open(votingSession.getOpen())
                .build();
    }
}
