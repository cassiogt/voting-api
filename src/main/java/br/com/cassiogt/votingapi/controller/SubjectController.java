package br.com.cassiogt.votingapi.controller;

import br.com.cassiogt.votingapi.dto.SubjectDTO;
import br.com.cassiogt.votingapi.service.SubjectService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import javax.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * This class is a endpoint to subjects API.
 */
@Slf4j
@RestController
@RequestMapping("/api/v1/subjects")
@Api(tags = "Subject")
public class SubjectController {

    private SubjectService subjectService;

    @Autowired
    public SubjectController(SubjectService subjectService) {
        this.subjectService = subjectService;
    }

    @PostMapping
    @ApiOperation(value = "Creates a subject to be voted")
    public ResponseEntity<SubjectDTO> createSubject(@Valid @RequestBody SubjectDTO subjectDTO) {
        log.trace("Creating subject {}", subjectDTO.getDescription());
        return ResponseEntity.status(HttpStatus.CREATED).body(subjectService.createSubject(subjectDTO));
    }
}
