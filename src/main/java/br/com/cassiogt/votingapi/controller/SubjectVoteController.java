package br.com.cassiogt.votingapi.controller;

import br.com.cassiogt.votingapi.dto.SubjectVoteDTO;
import br.com.cassiogt.votingapi.dto.VotingResultDTO;
import br.com.cassiogt.votingapi.service.SubjectVoteService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import javax.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * This class is a endpoint to voting API. Has a {@code POST} method to record a vote and a {@code GET} method to
 * return the result of a voting session.
 */
@Slf4j
@RestController
@RequestMapping("/api/v1/votes")
@Api(tags = "Votes")
public class SubjectVoteController {

    private SubjectVoteService subjectVoteService;

    @Autowired
    public SubjectVoteController(SubjectVoteService subjectVoteService) {
        this.subjectVoteService = subjectVoteService;
    }

    @PostMapping
    @ApiOperation(value = "Creates a new vote on a subject during a open session")
    public ResponseEntity vote(@Valid @RequestBody SubjectVoteDTO subjectVoteDTO) {
        log.trace("Member with CPF {} is voting", subjectVoteDTO.getCpf());
        this.subjectVoteService.vote(subjectVoteDTO);
        return new ResponseEntity(HttpStatus.CREATED);
    }

    @GetMapping(value = "/results/{subjectId}")
    @ApiOperation(value = "Returns the result of the subject voting")
    public ResponseEntity<VotingResultDTO> getVotingResults(@PathVariable("subjectId") Integer subjectId) {
        return ResponseEntity.ok(subjectVoteService.findVotingResultsBySubjectId(subjectId));
    }
}