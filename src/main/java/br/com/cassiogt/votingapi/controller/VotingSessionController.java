package br.com.cassiogt.votingapi.controller;

import br.com.cassiogt.votingapi.dto.OpenVotingSessionDTO;
import br.com.cassiogt.votingapi.dto.VotingSessionDTO;
import br.com.cassiogt.votingapi.service.VotingSessionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import javax.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * This class is a endpoint to voting sessions API. Has as single {@code POST} method to create a voting session.
 */
@Slf4j
@RestController
@RequestMapping("/api/v1/sessions")
@Api(tags = "Voting Session")
public class VotingSessionController {

    private final VotingSessionService votingSessionService;

    @Autowired
    public VotingSessionController(VotingSessionService votingSessionService) {
        this.votingSessionService = votingSessionService;
    }

    @PostMapping
    @ApiOperation(value = "Opens a new session for a subject during a limited period")
    public ResponseEntity<VotingSessionDTO> openVotingSession(
            @Valid @RequestBody OpenVotingSessionDTO openVotingSessionDTO) {
        log.trace("Try to open session for subject ID {}", openVotingSessionDTO.getSubjectId());
        VotingSessionDTO votingSessionDTO = this.votingSessionService.openVotingSession(openVotingSessionDTO);
        log.debug("Session for subject ID {} opened. Starts at {} and closes at {}", votingSessionDTO.getSubjectId(),
                votingSessionDTO.getBeginDate(), votingSessionDTO.getDueDate());
        return ResponseEntity.status(HttpStatus.CREATED).body(votingSessionDTO);
    }

}
