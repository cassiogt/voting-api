package br.com.cassiogt.votingapi.schedule;

import br.com.cassiogt.votingapi.dto.VotingSessionDTO;
import br.com.cassiogt.votingapi.entity.VotingSession;
import br.com.cassiogt.votingapi.service.VotingSessionService;
import java.time.LocalDateTime;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * Component that implements schedulers.
 */
@Slf4j
@Component
public class VotingScheduler {

    private VotingSessionService votingSessionService;

    @Autowired
    public VotingScheduler(VotingSessionService votingSessionService) {
        this.votingSessionService = votingSessionService;
    }

    /**
     * Voting session monitor, every 15s it verifies if
     * there is an {@link VotingSession} with expired time.
     * If there is, calls {@link VotingSessionService#closeSession(VotingSessionDTO)}
     * to close the respective session.
     */
    @Scheduled(fixedDelay = 15000)
    private void votingSessionMonitor() {
        //votingSessionService.closeExpiredSessions();
        List<VotingSessionDTO> list = votingSessionService.findOpenSessions();
        list.stream()
                .filter(votingSessionDto -> votingSessionDto.getDueDate().isBefore(LocalDateTime.now()))
                .forEach(votingSessionDto -> {
                    log.trace("Closing session with ID " + votingSessionDto.getId());
                    votingSessionService.closeSession(votingSessionDto);
                });
    }
}