package br.com.cassiogt.votingapi.entity;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * This class is a representation of voting_session table.
 * It stores the begin and end of a voting session created for a specific {@link Subject}.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "voting_session")
public class VotingSession {

    @Id
    @GeneratedValue
    private Integer id;

    @Column(name = "subject_id", nullable = false)
    private Integer subjectId;

    @Column(name = "begin_time", nullable = false)
    private LocalDateTime beginTime;

    @Column(name = "due_time", nullable = false)
    private LocalDateTime dueTime;

    @Builder.Default
    @Column(name = "open", nullable = false)
    private Boolean open = Boolean.FALSE;

}