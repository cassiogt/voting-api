package br.com.cassiogt.votingapi.service;

import br.com.cassiogt.votingapi.entity.MemberVote;
import br.com.cassiogt.votingapi.repository.MemberVoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Implements business logic for a Member vote.
 */
@Service
public class MemberVoteService {

    private final MemberVoteRepository memberVoteRepository;

    @Autowired
    public MemberVoteService(MemberVoteRepository memberVoteRepository) {
        this.memberVoteRepository = memberVoteRepository;
    }

    /**
     * Verifies if a member has already voted for a {@link br.com.cassiogt.votingapi.entity.Subject}
     *
     * @param cpf       the CPF from the member.
     * @param subjectId the {@link br.com.cassiogt.votingapi.entity.Subject} ID.
     * @return {@code true} if the member already voted, or {@code false}, otherwise.
     */
    @Transactional(readOnly = true)
    public boolean hasMemberVotedForSubjectId(String cpf, Integer subjectId) {
        return memberVoteRepository.existsByCpfAndSubjectId(cpf, subjectId);
    }

    /**
     * Creates a new tuple of {@link MemberVote}, storing that the member
     * votes for the subject passed by parameter.
     *
     * @param cpf       the CPF from the member.
     * @param subjectId the subject ID voted for.
     */
    @Transactional
    public void saveMemberVoteForSubjectId(String cpf, Integer subjectId) {
        memberVoteRepository.save(MemberVote.builder()
                .cpf(cpf)
                .subjectId(subjectId)
                .build());
    }
}
