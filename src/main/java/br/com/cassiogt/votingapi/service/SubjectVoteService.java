package br.com.cassiogt.votingapi.service;

import br.com.cassiogt.votingapi.client.CpfValidationClient;
import br.com.cassiogt.votingapi.dto.SubjectDTO;
import br.com.cassiogt.votingapi.dto.SubjectVoteDTO;
import br.com.cassiogt.votingapi.dto.VotingResultDTO;
import br.com.cassiogt.votingapi.dto.VotingSessionDTO;
import br.com.cassiogt.votingapi.exception.AlreadyExistsException;
import br.com.cassiogt.votingapi.exception.ForbiddenException;
import br.com.cassiogt.votingapi.exception.InvalidSessionException;
import br.com.cassiogt.votingapi.repository.SubjectVoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class SubjectVoteService {

    private final VotingSessionService votingSessionService;
    private final MemberVoteService memberVoteService;
    private final SubjectVoteRepository subjectVoteRepository;
    private final SubjectService subjectService;
    private final CpfValidationClient cpfValidation;

    @Autowired
    public SubjectVoteService(VotingSessionService votingSessionService,
                              MemberVoteService memberVoteService,
                              SubjectVoteRepository subjectVoteRepository,
                              SubjectService subjectService,
                              CpfValidationClient cpfValidation) {
        this.votingSessionService = votingSessionService;
        this.memberVoteService = memberVoteService;
        this.subjectVoteRepository = subjectVoteRepository;
        this.subjectService = subjectService;
        this.cpfValidation = cpfValidation;
    }

    public void vote(SubjectVoteDTO subjectVoteDTO) {
        VotingSessionDTO votingSessionDTO = votingSessionService.findBySubjectId(subjectVoteDTO.getSubjectId());
        if (!Boolean.TRUE.equals(votingSessionDTO.getOpen())) {
            throw new InvalidSessionException("Session is already closed");
        }

        if (memberVoteService.hasMemberVotedForSubjectId(subjectVoteDTO.getCpf(), subjectVoteDTO.getSubjectId())) {
            throw new AlreadyExistsException("Member already voted in this session");
        }

        if (!cpfValidation.isMemberAuthorizedToVote(subjectVoteDTO.getCpf())) {
            throw new ForbiddenException("Member not authorized to vote");
        }

        /* The member vote and vote subject are stored in separated tables do avoid reference the member to it`s vote.
        Unfortunately, this schema wil create a tuple with same ID will be created on each table, this needs to be
        improved. */
        memberVoteService.saveMemberVoteForSubjectId(subjectVoteDTO.getCpf(), subjectVoteDTO.getSubjectId());
        subjectVoteRepository.save(subjectVoteDTO.asEntity());
    }

    @Transactional
    public VotingResultDTO findVotingResultsBySubjectId(Integer subjectId) {
        SubjectDTO subjectDTO = subjectService.findById(subjectId);
        VotingSessionDTO votingSessionDTO = votingSessionService.findBySubjectId(subjectDTO.getId());
        if (Boolean.TRUE.equals(votingSessionDTO.getOpen())) {
            throw new InvalidSessionException("Session is still open, try again later");
        }
        VotingResultDTO result = new VotingResultDTO();
        result.setSubject(subjectDTO);
        subjectVoteRepository.findAllBySubjectId(subjectId).forEach(vote -> {
            if (Boolean.TRUE.equals(vote.getVote())) {
                result.incrementPros();
            } else {
                result.incrementCons();
            }
        });
        return result;
    }
}
