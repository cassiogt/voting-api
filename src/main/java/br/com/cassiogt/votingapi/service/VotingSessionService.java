package br.com.cassiogt.votingapi.service;

import br.com.cassiogt.votingapi.dto.OpenVotingSessionDTO;
import br.com.cassiogt.votingapi.dto.SubjectDTO;
import br.com.cassiogt.votingapi.dto.VotingSessionDTO;
import br.com.cassiogt.votingapi.entity.VotingSession;
import br.com.cassiogt.votingapi.exception.AlreadyExistsException;
import br.com.cassiogt.votingapi.exception.DataNotFoundException;
import br.com.cassiogt.votingapi.repository.VotingSessionRepository;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Implements business logic for a Voting Session.
 */
@Slf4j
@Service
public class VotingSessionService {

    @Value("${app.voting-duration:1}")
    private String votingDuration;

    private final VotingSessionRepository votingSessionRepository;
    private final SubjectService subjectService;

    @Autowired
    public VotingSessionService(VotingSessionRepository votingSessionRepository, SubjectService subjectService) {
        this.votingSessionRepository = votingSessionRepository;
        this.subjectService = subjectService;
    }

    /**
     * Creates a session for a subject with limited interval.
     * <p>Receives a {@link OpenVotingSessionDTO} that contains an ID of a
     * {@link br.com.cassiogt.votingapi.entity.Subject} and a duration time which represents the period that the
     * session keeps open.
     * Validates if the subject exists and if there is no session created for, if there is, throws an
     * {@link AlreadyExistsException}, otherwise, creates and stores a new instance of {@link VotingSession}
     *
     * @param openVotingSessionDTO the payload with subject and duration information.
     * @return a new instance of {@link VotingSessionDTO}.
     */
    @Transactional
    public VotingSessionDTO openVotingSession(OpenVotingSessionDTO openVotingSessionDTO) {
        SubjectDTO subjectDTO = this.subjectService.findById(openVotingSessionDTO.getSubjectId());
        Optional<VotingSession> votingSessionOptional = votingSessionRepository.findBySubjectId(subjectDTO.getId());
        if (votingSessionOptional.isPresent()) {
            throw new AlreadyExistsException("Voting session already created for subject " + subjectDTO.getDescription());
        }
        log.trace("Subject present, creating session");
        VotingSession votingSession = VotingSession.builder()
                .subjectId(subjectDTO.getId())
                .beginTime(LocalDateTime.now())
                .dueTime(calculateDueTime(openVotingSessionDTO.getDuration()))
                .open(Boolean.TRUE)
                .build();
        return VotingSessionDTO.fromEntity(this.votingSessionRepository.save(votingSession));
    }

    /**
     * Calculates and returns the time the session will be closed. If there is no duration, the session defaults to a
     * period of 1 minute or X minutes, stored on applications.yml.
     *
     * @param durationInMinutes the period of the session.
     * @return now plus duration date time.
     */
    private LocalDateTime calculateDueTime(Integer durationInMinutes) {
        if (Objects.nonNull(durationInMinutes) && durationInMinutes > 0) {
            return LocalDateTime.now().plusMinutes(durationInMinutes);
        } else {
            return LocalDateTime.now().plusMinutes(Integer.valueOf(votingDuration));
        }
    }

    /**
     * Searches for open sessions.
     *
     * @return a list of {@link VotingSessionDTO} that are still open.
     */
    @Transactional(readOnly = true)
    public List<VotingSessionDTO> findOpenSessions() {
        return votingSessionRepository.findByOpenEquals(Boolean.TRUE)
                .stream()
                .map(VotingSessionDTO::fromEntity)
                .collect(Collectors.toList());
    }

    /**
     * Closes a session. Setting the flag {@code open} to {@code false}.
     *
     * @param votingSessionDTO the voting session to be closed.
     */
    @Transactional
    public void closeSession(VotingSessionDTO votingSessionDTO) {
        Optional<VotingSession> votingSessionOptional = votingSessionRepository.findById(votingSessionDTO.getId());
        if (!votingSessionOptional.isPresent()) {
            throw new DataNotFoundException("Voting session not found for ID " + votingSessionDTO.getId());
        }
        VotingSession votingSession = votingSessionOptional.get();
        votingSession.setOpen(Boolean.FALSE);
        votingSessionRepository.save(votingSession);
    }

    /**
     * Searches for a Voting session with specific {@link br.com.cassiogt.votingapi.entity.Subject} and returns it,
     * or throws a {@link DataNotFoundException} otherwise.
     *
     * @param subjectId the session subject ID.
     * @return an instance of {@code VotingSessionDTO}.
     */
    @Transactional(readOnly = true)
    public VotingSessionDTO findBySubjectId(Integer subjectId) {
        Optional<VotingSession> votingSessionOptional = votingSessionRepository.findBySubjectId(subjectId);
        if (votingSessionOptional.isEmpty()) {
            throw new DataNotFoundException("There is no VotingSession for subject id " + subjectId);
        }
        return VotingSessionDTO.fromEntity(votingSessionOptional.get());
    }
}