package br.com.cassiogt.votingapi.service;

import br.com.cassiogt.votingapi.dto.SubjectDTO;
import br.com.cassiogt.votingapi.entity.Subject;
import br.com.cassiogt.votingapi.exception.DataNotFoundException;
import br.com.cassiogt.votingapi.repository.SubjectRepository;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Implements business logic for a Subject.
 */
@Service
public class SubjectService {

    private SubjectRepository subjectRepository;

    @Autowired
    public SubjectService(SubjectRepository subjectRepository) {
        this.subjectRepository = subjectRepository;
    }

    /**
     * Creates a new subject and stores on the database.
     *
     * @param subjectDTO the {@link SubjectDTO} payload.
     * @return the same {@link SubjectDTO} but with ID.
     */
    @Transactional
    public SubjectDTO createSubject(SubjectDTO subjectDTO) {
        return SubjectDTO.fromEntity(subjectRepository.save(subjectDTO.asEntity()));
    }

    /**
     * Searches for a {@link Subject} with ID passed by parameter. If found, returns an instance of
     * {@link SubjectDTO}, if not, throws a {@link DataNotFoundException}.
     *
     * @param id the subject id that is being searched for.
     * @return a {@link SubjectDTO} if found.
     */
    @Transactional(readOnly = true)
    public SubjectDTO findById(Integer id) {
        Optional<Subject> subjectOptional = subjectRepository.findById(id);
        if (subjectOptional.isEmpty()) {
            throw new DataNotFoundException("Subject not found for ID " + id);
        }
        return SubjectDTO.fromEntity(subjectOptional.get());
    }

}
