package br.com.cassiogt.votingapi.client;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

/**
 * This class consists on a single method that make calls to an
 * external web service and validates CPF (Cadastro de Pessoa Física) numbers.
 */
@Slf4j
@Component
public class CpfValidationClient {

    private static final String CLIENT_URL = "https://user-info.herokuapp.com/users/{cpf}";
    private static final String ABLE_TO_VOTE = "ABLE_TO_VOTE";
    private final ObjectMapper objectMapper = new ObjectMapper();

    /**
     * Verifies if a CPF, passed by parameter, is able to vote or not.
     * It calls a web service which return {@code ABLE_TO_VOTE}
     * if it is, or {@code UNABLE_TO_VOTE} otherwise.
     *
     * @param cpf the CPF that will be verified.
     * @return {@code true} if CPF is valid.
     */
    public boolean isMemberAuthorizedToVote(String cpf) {

        try {
            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<String> response = restTemplate.getForEntity(CLIENT_URL, String.class, cpf);

            return HttpStatus.OK.equals(response.getStatusCode())
                    && ABLE_TO_VOTE.equals(objectMapper.readTree(response.getBody()).path("status").textValue());

        } catch (JsonProcessingException | RestClientException e) {
            log.error("Failed to request validation", e);
        }

        return Boolean.FALSE;

    }
}
