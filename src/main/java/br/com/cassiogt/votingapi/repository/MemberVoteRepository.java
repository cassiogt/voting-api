package br.com.cassiogt.votingapi.repository;

import br.com.cassiogt.votingapi.entity.MemberVote;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MemberVoteRepository extends JpaRepository<MemberVote, Integer> {

    Boolean existsByCpfAndSubjectId(String cpf, Integer subjectId);

}
