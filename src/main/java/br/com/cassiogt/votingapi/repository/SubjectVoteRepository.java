package br.com.cassiogt.votingapi.repository;

import br.com.cassiogt.votingapi.entity.SubjectVote;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SubjectVoteRepository extends JpaRepository<SubjectVote, Integer> {

    List<SubjectVote> findAllBySubjectId(Integer subjectId);
}
