package br.com.cassiogt.votingapi.repository;

import br.com.cassiogt.votingapi.entity.VotingSession;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface VotingSessionRepository extends JpaRepository<VotingSession, Integer> {

    Optional<VotingSession> findBySubjectId(Integer id);

    boolean existsById(Integer id);

    @Query("select vs from VotingSession vs where vs.open = :open")
    List<VotingSession> findByOpenEquals(@Param("open") Boolean open);
}
