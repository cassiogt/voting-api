package br.com.cassiogt.votingapi.controller;

import br.com.cassiogt.votingapi.dto.SubjectDTO;
import br.com.cassiogt.votingapi.service.SubjectVoteService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * This class implements subject API tests using and Embedded database.
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SubjectIntegrationTests {

    private static final String SUBJECTS_URL = "/api/v1/subjects";

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void shouldAddSubjectAndReturnWithID() {
        String description = "Subject test";
        ResponseEntity<SubjectDTO> responseEntity = restTemplate.postForEntity(SUBJECTS_URL,
                SubjectDTO.builder().description(description).build(),
                SubjectDTO.class);
        assertEquals(responseEntity.getStatusCode(), HttpStatus.CREATED, "HTTP response should be 201");
        SubjectDTO subjectDTO = responseEntity.getBody();
        assertNotNull(subjectDTO, "Subject shouldn't be null");
        assertNotNull(subjectDTO.getId(), "Subject ID shouldn't be null");
        assertEquals(subjectDTO.getDescription(), description, "Subject subject differs from informed subject");
    }

}
