package br.com.cassiogt.votingapi.service;

import br.com.cassiogt.votingapi.client.CpfValidationClient;
import br.com.cassiogt.votingapi.dto.SubjectVoteDTO;
import br.com.cassiogt.votingapi.dto.VotingSessionDTO;
import br.com.cassiogt.votingapi.entity.SubjectVote;
import br.com.cassiogt.votingapi.repository.SubjectVoteRepository;
import java.time.LocalDateTime;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

/**
 * This class implements tests for {@link SubjectVoteService} using Mock.
 */
@SpringBootTest
class SubjectVoteServiceTests {

    @Mock
    CpfValidationClient cpfValidation;
    @Mock
    VotingSessionService votingSessionService;
    @Mock
    MemberVoteService memberVoteService;
    @Mock
    SubjectVoteRepository subjectVoteRepository;
    @Mock
    SubjectService subjectService;

    SubjectVoteService subjectVoteService;

    @BeforeEach
    void init() {
        subjectVoteService = new SubjectVoteService(votingSessionService, memberVoteService,
                subjectVoteRepository,
                subjectService, cpfValidation);
    }

    @Test
    void givenSubjectVoteWhenSaveVoteThenSuccess() {
        SubjectVoteDTO voteDTO =
                SubjectVoteDTO.builder().cpf("01065588038").subjectId(1).vote(Boolean.TRUE).build();
        mockValidSession();
        mockValidMemberCpf();
        mockMemberWithoutVotes();

        SubjectVote subjectVote = voteDTO.asEntity();
        subjectVote.setId(1);
        when(subjectVoteRepository.save(any(SubjectVote.class))).thenReturn(subjectVote);

        assertDoesNotThrow(() -> subjectVoteService.vote(voteDTO));
    }

    private void mockValidSession() {
        when(votingSessionService.findBySubjectId(any(Integer.class)))
                .thenReturn(VotingSessionDTO.builder().id(1).beginDate(LocalDateTime.now())
                        .dueDate(LocalDateTime.now().plusMinutes(2)).open(Boolean.TRUE).build());
    }

    private void mockValidMemberCpf() {
        when(cpfValidation.isMemberAuthorizedToVote(any(String.class)))
                .thenReturn(Boolean.TRUE);
    }

    private void mockMemberWithoutVotes() {
        when(memberVoteService.hasMemberVotedForSubjectId(any(String.class), any(Integer.class)))
                .thenReturn(Boolean.FALSE);
    }

}