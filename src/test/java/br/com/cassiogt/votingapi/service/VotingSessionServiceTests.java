package br.com.cassiogt.votingapi.service;

import br.com.cassiogt.votingapi.dto.OpenVotingSessionDTO;
import br.com.cassiogt.votingapi.dto.SubjectDTO;
import br.com.cassiogt.votingapi.dto.VotingSessionDTO;
import br.com.cassiogt.votingapi.entity.VotingSession;
import br.com.cassiogt.votingapi.exception.AlreadyExistsException;
import br.com.cassiogt.votingapi.repository.VotingSessionRepository;
import java.time.LocalDateTime;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

/**
 * This class implements tests for {@link VotingSessionService} using Mock.
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class VotingSessionServiceTests {

    @Mock
    VotingSessionRepository votingSessionRepository;
    @Mock
    SubjectService subjectService;

    VotingSessionService votingSessionService;

    VotingSession session = VotingSession.builder()
            .id(1)
            .subjectId(1)
            .beginTime(LocalDateTime.now())
            .dueTime(LocalDateTime.now().plusMinutes(1))
            .open(Boolean.TRUE)
            .build();

    @BeforeEach
    void beforeAll() {

        votingSessionService = new VotingSessionService(votingSessionRepository, subjectService);

        when(votingSessionRepository.save(any(VotingSession.class)))
                .thenReturn(session);
    }

    @Test
    public void onOpenVotingSessionWithValidSubjectReturnSuccess() {

        when(subjectService.findById(any(Integer.class)))
                .thenReturn(SubjectDTO.builder().id(1).description("TEST").build());

        when(votingSessionRepository.findBySubjectId(any(Integer.class)))
                .thenReturn(Optional.empty());

        VotingSessionDTO votingSessionDTO = votingSessionService.openVotingSession(new OpenVotingSessionDTO(1, 1));
        assertNotNull(votingSessionDTO, "votingSessionDTO shouldn't be null");
        assertEquals(1, votingSessionDTO.getSubjectId(), "Expected Subject ID was 1");
    }

    @Test
    public void onOpenVotingSessionWithAlreadyVotedSubjectThrowException() {

        when(subjectService.findById(any(Integer.class)))
                .thenReturn(SubjectDTO.builder().id(1).description("TEST").build());

        when(votingSessionRepository.findBySubjectId(any(Integer.class)))
                .thenReturn(Optional.of(session));

        assertThrows(AlreadyExistsException.class,
                () -> votingSessionService.openVotingSession(new OpenVotingSessionDTO(1, 1)),
                "Open voting should thown exception");
    }

}
